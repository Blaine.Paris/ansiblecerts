[NBNCO\blaineparris@svdam3100001ee debug]$ cat  ~/scripts/gen_csr.sh
clear
. /home/NBNCO/blaineparris/scripts/script_vars
echo ${ST}

echo "Processing for ${ST}"

echo keytool -certreq -storepass "**" -keyalg RSA -alias tomcat -file ${CSRFILE} -keystore ${KSFILE}
keytool -certreq -storepass ${STOREPASS} -keyalg RSA -alias tomcat -file ${CSRFILE} -keystore ${KSFILE}

#Valiate
keytool -list -storepass ${STOREPASS} -keystore ${KSFILE}
openssl req -noout -text -in ${CSRFILE} | egrep "DNS|Subject"

echo "

"
echo "                  ##################################################"
echo "                     Certificate Request I N S T R U C T I O N S"
echo "                  ##################################################"
echo "

"
echo "1. For *DNS Name Use : `tput bold`${FQDN}`tput sgr0`"
echo "2. For *Additional DNS Names Use : `tput bold`${SHOST}`tput sgr0`"
echo "3. Copy and Paste the following into the box that says *Paste CSR below:"
tput bold
cat ${CSRFILE}
tput sgr0
echo "

"
echo "For Production NBN certificate Requests use this link `tput smul` https://pki.symauth.com/certificate-service/?ac=516719&pf=2.16.840.1.113733.1.16.1.5.1.2.69508833 `tput rmul`"
echo "For Non-Production NBN Certificate Requests use this link `tput smul` https://pki.symauth.com/certificate-service/?ac=516719&pf=2.16.840.1.113733.1.16.1.5.1.2.68857288 `tput rmul`"


